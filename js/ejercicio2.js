/*2-  Crear un script que solicite al usuario mediante un prompt el nombre de ciudades y almacenarlas en un arreglo, cuando el usuario seleccione cancelar o ingrese el valor “0” se debe mostrar el arreglo generado, luego realizar las siguientes acciones:

Mostrar la longitud del arreglo.
Mostrar en el documento web los ítems de las posiciones primera, tercera y última.
Añade en última posición la ciudad de París.
Escribe por pantalla el elemento que ocupa la segunda posición.
Sustituye el elemento que ocupa la segunda posición por la ciudad de 'Barcelona'.
*/

let ciudades=[];

do{
    var opcion = confirm("Clicka en Aceptar o Cancelar");
    if (opcion == true) {
        ciudades.push(prompt("Ingrese una ciudad"));
	} else {
        for(let i=0;i<ciudades.length;i++)
        {
            document.write(ciudades[i]+"<br>");
        }
	}
}while(opcion==true);

ciudades.push("Paris");

let longitud=(ciudades.length);
document.write(ciudades[longitud - 1]);

document.write("<br>La longitud del array es de " + longitud);

document.write("<br>"+"El primer elemento de mi arreglo es " + ciudades[0] + "<br>");
document.write("El tercer elemento de mi arreglo es " + ciudades[2] + "<br>");
document.write("El ultimo elemento de mi arreglo es " + ciudades[longitud - 1] + "<br>");

document.write("El segundo elemento de mi arreglo es " + ciudades[1] + "<br>");

ciudades[1]="Barcelona";

document.write("<br> Escribimos de nuevo el array reemplazando por barcelona a la segunda posicion <br>");
for(let i=0;i<ciudades.length;i++)
        {
            document.write(ciudades[i]+"<br>");
        }